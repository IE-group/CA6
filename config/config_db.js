let Sequelize = require ('sequelize');

const sequelize = new Sequelize('database', null, null, {
    dialect: 'sqlite',
    // SQLite only
    storage: 'database.sqlite',

});

module.exports = sequelize;
