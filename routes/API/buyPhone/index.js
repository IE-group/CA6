let express = require("express");
const router = express.Router();
let House = require('../../../Models/House');
let Individual = require('../../../Models/Individual');
let HouseUser = require('../../../Models/HouseUser').HouseUser;


router.get('/', async function (req, response) {
    let is = await Individual.find({where: {id: 1}});
    let h = await House.find({where: {id: req.param("id")}})
    if (is.balance >= 1000) {
        await HouseUser.build({houseId: req.param("id"), userId: 1}).save();
        let new_balance = is.balance - 1000;
        await Individual.update({balance: new_balance}, {where: {id: 1}});
    } else {
        console.log("Insuficient");
    }
    let redir = 'house/info?id=' + h.id;
    return response.redirect(redir);
});

module.exports = router;

