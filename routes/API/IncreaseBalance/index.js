let express = require("express");
const router = express.Router();
let Individual = require('../../../Models/Individual');


let request = require('request');


// POST method route
router.post('/', async function (req, response) {
    const money = parseInt(req.body.money);
    let result;
    let data = {
        "userId": "123",
        "value": money
    };
    await request({
        headers: {
            'apiKey': "60ffead0-12e7-11e8-87b4-496f79ef1988",
        },
        uri: 'http://139.59.151.5:6664/bank/pay',
        form: data,
        method: 'POST'
    }, async function (err, res) {
        if (!err && res.statusCode === 200) {
            result = {result: "OK"};

            let is = await Individual.find({where: {id: 1}});
            let new_balance = parseInt(is.balance) + money;
            await Individual.update({balance: new_balance}, {where: {id: 1}});

            response.send({"result": "OK", "credit": new_balance});

        }
        else {

            result = {result: "ERROR 500: Please Try Again!"};
            response.send(result);
        }
    });

});

module.exports = router;
