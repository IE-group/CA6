const express = require("express");
const router = express.Router();
let House = require('../../../Models/House');
let Individual = require('../../../Models/Individual');
const HouseUser = require('../../../Models/HouseUser').HouseUser;

let RealState = require('../../../Models/RealState');
let Utils = require('../../../Utils/Utils');


router.get('/', async function (req, res) {
    await HouseUser.sync({force: true});
    await House.sync({force: true});
    await Individual.sync({force: true});
    await RealState.sync({force: true});
    await Individual.create({
        id: 1,
        name: "بهنام همایون",
        phone: "123456",
        balance: 1000,
        userName: "BH",
        Password: "A"
    });

    await House.create({
        id: "1",
        area: 200,
        ownerId: 0,
        sellPrice: 0,
        basePrice: 0,
        rentPrice: 200,
        buildingType: 0,
        address: "",
        dealType: "0",
        imageURL: "",
        description: "",
        phone: "1232131"
    });
    await HouseUser.buy_house("1", 1);

    await RealState.bulkCreate([
        {id: 0, name: "default", url: "", expireTime: ""},
        {id: 1, name: "KhaneBeDoosh", url: "http://139.59.151.5:6664/khaneBeDoosh/v2/house", expireTime: ""}

    ]).catch(errors => {
    });
    res.send("DONE");
});
module.exports = router;
