const express = require("express");
const router = express.Router();

const house = require('./House');
const increase = require('./IncreaseBalance');
const init = require('./Init');
const buyPhone = require('./buyPhone');

router.get('/', function (req, res) {
    res.end('All Systems Green!')
});

router.use('/house', house);
router.use('/increase', increase);
router.use('/init', init);
router.use('/buyPhone', buyPhone);

module.exports = router;
