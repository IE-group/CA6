let express = require("express");
let router = express.Router();
const House = require('./House');


router.get('/', async function (req, res) {
    await House.search_house(req, res);
});

router.get('/info', async function (req, res) {
    await House.get_houseInfo(1, req.query.id, res);
});


module.exports = router;
