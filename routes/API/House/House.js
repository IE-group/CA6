let request = require('async-request');
let Utils = require('../../../Utils/Utils');
let RealState = require('../../../Models/RealState');


async function get_houseInfo(userId, houseId, res) {

    let response = {};
    let result;
    let house;
    let purchased;
    let wealthy;
    const logged = userId !== -1;
    if (!logged) {
        res.statusCode = 401;
    }
    result = await Utils.search_house_in_db(houseId);

    if (!result) {
        response["status"] = "NOK";
        res.statusCode = 404;
        res.send(response)
    }


    let url = await Utils.fetch_url(result.ownerId);
    if (!url) {
        house = result.dataValues;
    }
    else {

        result = await fetch_house(url, houseId);
        house = await JSON.parse(result).data;
    }
    if (logged) {
        wealthy = await Utils.check_wealthy(userId);
        purchased = await Utils.check_purchased(userId, houseId);
    }
    if (logged && purchased)
        house["paid"] = true;
    else {
        house["phone"] = await Utils.fake_generator(house.phone);
        house["paid"] = false;
        house["afford"] = wealthy;

    }

    house["status"] = "OK";

    res.send(house);

}

async function fetch_house(url, houseId) {
    let result = "";
    houseId = "/" + houseId;
    let response;
    response = await request(url + houseId, async function (err, res) {

        if (!err && res.statusCode === 200) {
            result = res.body;
        }
        return res.body
    });

    return response.body;
}

async function get_houses(ownerId) {


    let url = await Utils.fetch_url(ownerId);

    let response = await request(url, function (err, res) {
        if (!err && res.statusCode === 200)
            return res.data;
    });
    return JSON.parse(response.body)
}

async function update_houses() {

    let owners = await RealState.findAll();

    owners.forEach(function (owner) {
        if (owner.id !== 0) {
            get_houses(owner.id)
                .then(async function (houses) {
                    if (houses.expireTime < owner.expireTime || !owner.expireTime.length) {
                        let homes = houses.data;
                        Utils.set_prices(homes, owner.id);
                        Utils.upsert_house(homes).then(async function () {
                            await owner.update({
                                expireTime: houses.expireTime
                            })
                        });
                    }
                })
        }
    })
}

async function search_house(req, res) {
    let result = {};
    update_houses()
        .then(async () => {
            const minArea = req.query["minArea"];
            const maxPrice = req.query["maxPrice"];
            let houseType = req.query["houseType"];
            houseType = houseType === "ویلایی" ? "0" : houseType === "آپارتمان" ? "1" : "";
            const dealType = req.query["dealType"];
            Utils.pick_houses_from_db(minArea, maxPrice, houseType, dealType)
                .then(async function (houses) {

                    if (!houses || !houses.length)
                        result["isFound"] = false;

                    else {
                        houses = await set_prices(houses);
                        result["isFound"] = true;
                        result["status"] = "OK";
                        result["houses"] = houses;
                    }
                    res.send(result);
                });
        });

}


async function set_prices(houses) {
    await houses.forEach(async function (house) {
        let price = {};
        price["sellPrice"] = house.sellPrice;
        price["rentPrice"] = house.rentPrice;
        price["basePrice"] = house.basePrice;

        house.dataValues["price"] = price;

        delete house.dataValues["sellPrice"];
        delete house.dataValues["basePrice"];
        delete house.dataValues["rentPrice"];
    });
    return houses
}

module.exports = {
    get_houseInfo,
    search_house
};