const express = require('express');
const router = express.Router();
const api = require('./API/index');
/* GET home page. */
router.get('/', function (req, res) {
    res.send('index');
});

router.use('/api', api);

module.exports = router;
