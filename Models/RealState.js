let sequelize = require('../config/config_db');
let Sequelize = require('sequelize');


const RealState = sequelize.define('RealState', {
    id:
        {
            type: Sequelize.INTEGER,
            primaryKey: true
        },
    name: Sequelize.STRING,
    url: Sequelize.STRING,
    expireTime: Sequelize.INTEGER
});

module.exports = RealState;
