let sequelize = require('../config/config_db');
let Sequelize = require('sequelize');

const House = sequelize.define('House', {
    id:
        {
            type: Sequelize.STRING,
            primaryKey: true
        },
    area: Sequelize.INTEGER,
    ownerId: Sequelize.INTEGER,
    sellPrice: Sequelize.INTEGER,
    basePrice: Sequelize.INTEGER,
    rentPrice: Sequelize.INTEGER,
    buildingType: Sequelize.STRING,
    address: Sequelize.STRING,
    dealType: Sequelize.INTEGER,
    imageURL: Sequelize.STRING,
    description: Sequelize.STRING,
    phone: Sequelize.STRING
});







module.exports = House;
