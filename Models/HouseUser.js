let sequelize = require('../config/config_db');
let Sequelize = require('sequelize');

const HouseUser = sequelize.define('House_User', {
    houseId: {
        type: Sequelize.STRING,
        primaryKey: true
    },
    userId: {
        type: Sequelize.INTEGER,
        primaryKey: true
    }
});

async function buy_house(userId, houseId) {
    await HouseUser.create({
        houseId: houseId,
        userId: userId
    });
}

module.exports = {
    HouseUser,
    buy_house,

};

