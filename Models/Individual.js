let sequelize = require('../config/config_db');
let Sequelize = require('sequelize');


const Individual = sequelize.define('Individual', {
    id:
        {
            type: Sequelize.INTEGER,
            primaryKey: true
        },
    name: Sequelize.STRING,
    phone: Sequelize.STRING,
    balance: Sequelize.INTEGER,
    userName: Sequelize.STRING,
    Password: Sequelize.STRING
});

module.exports = Individual;
