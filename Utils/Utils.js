let Individual = require('../Models/Individual');
let HouseUser = require('../Models/HouseUser').HouseUser;
const House = require('../Models/House');
let RealState = require('../Models/RealState');
const sequelize = require("sequelize");
let Op = sequelize.Op;

async function fake_generator(phone) {
    let res;
    const len = phone.length;
    if (len < 2)
        res = "********";
    else if (len < 4)
        res = phone.substring(0, 1) + "***" + phone.substring(3, 4);
    else
        res = phone.substring(0, 2) + "******" + phone.substring(len - 1, len);

    return res;
}

async function check_purchased(userId, houseId) {

    let row = await HouseUser.findAll({
        where: {
            userId: userId,
            houseId: houseId
        }
    }).catch(errors => {
        return false;
    });
    console.log("HOUSE USER: ", row);
    return !!row.length;

}

async function check_wealthy(userId) {

    let user = await Individual.findAll({where: {id: userId}});

    return parseInt(user[0].dataValues.balance) >= 1000;
}

async function fetch_url(ownerId) {
    let rs = await RealState.findAll({
        where: {
            id: ownerId
        }
    });

    return rs[0].url;
}

function set_prices(houses, ownerId) {

    houses.forEach(function (house) {
        house.sellPrice = house.price.sellPrice;
        house.rentPrice = house.price.rentPrice;
        house.basePrice = house.price.basePrice;
        house.ownerId = ownerId;

        house.buildingType = house.buildingType === "ویلایی" ? 0 : 1;
        delete house["price"];
    });
    // return houses;
}

async function upsert_house(houses) {

    return House
        .findOne({
            where: {
                id: houses[0].id
            }
        })
        .then(async function (obj) {

            if (obj) { // update
                await update_houses(houses);
            }
            else { // insert
                await House.bulkCreate(houses);

            }
        });
}
async function search_house_in_db(houseId) {
    const h = await House.findAll({
        where: {
            id: houseId
        }
    });
    if (h.length)
        return h[0];
    else
        return null;
}

async function pick_houses_from_db(minArea, maxPrice, houseType, dealType) {
    maxPrice = parseInt(maxPrice);
    minArea = parseInt(minArea);
    dealType = parseInt(dealType);

    return House.findAll({
        where: {
            area: {
                [Op.gt]: minArea,
            },
            [Op.or]: [
                {
                    buildingType: houseType.length ? houseType : 0,
                    sellPrice: {
                        [Op.lt]: maxPrice
                    }
                }, {
                    buildingType: houseType.length ? houseType : 1,
                    rentPrice: {
                        [Op.lt]: maxPrice
                    }
                }
            ],
            dealType: dealType,
            [Op.or]:
                [{
                    buildingType: houseType.length ? houseType : 0
                }, {
                    buildingType: houseType.length ? houseType : 1
                }]
        }
    })
        .then(function (houses) {
            if (!houses || !houses.length)
                return null;
            else
                return houses;
        });
}

async function update_houses(houses) {
    await houses.forEach(function (house) {
        const id = house.id;
        Individual.update(
            house,
            {
                where: {
                    id: id
                }
            })
    })
}


module.exports = {
    fake_generator,
    check_purchased,
    check_wealthy,
    fetch_url,
    set_prices,
    upsert_house,
    pick_houses_from_db,
    search_house_in_db,
};